from flask import Flask, render_template, request, jsonify
import pandas as pd
import pickle


app = Flask(__name__)

# load data
movie_cols = ['movie id', 'movie title', 'release date', 'video release date', 'IMDb URL',
              'unknown', 'Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime',
              'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery',
              'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']

movies = pd.read_csv('ml-100k/ml-100k/u.item', sep='|', names=movie_cols, encoding='latin-1')
# ratings = pd.read_csv("1000_mv_rating.csv")

movie_title = list(movies['movie title'])
movie_genres = ['Action', 'Adventure', 'Animation', 'Children\'s', 'Comedy', 'Crime',
              'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery',
              'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']
# Load the saved model from the file called `my_model.pkl`
with open('dict_sim_mv', 'rb') as file:
    my_model = pickle.load(file)

# load the template
@app.route('/')
def home():
    return render_template('index.html', movie_titles=movie_title, movie_genres = movie_genres )

# recommend movies
def get_similar_movies(movie_id):
    similar_movies = list(my_model[movie_id])
    return similar_movies

# get movie details
def get_movie_details(movie_id):
    movie = movies[movies['movie id'] == movie_id]
    movie_title = movie['movie title'].values[0]

    column_names = movie.columns
    genre = []
    for gen in column_names[6:]:
        if movie[gen].values[0] == 1:
            genre.append(gen)
    movie_release_date = movie['release date'].values[0]
    imdb_url = movie['IMDb URL'].values[0]
    # movie_imdb_id = movie['imdb_id'].values[0]
    # return movie_title, movie_genres, movie_poster, movie_imdb_id
    return movie_title, genre, movie_release_date, imdb_url


@app.route('/recommend', methods=['POST'])
def recommend_movies():
    movie_name = request.form['movie']
    movie_id = movies[movies['movie title'] == movie_name]['movie id'].values[0]
    recommended_movie_ids = get_similar_movies(movie_id)
    recommended_movies = []
    for id in recommended_movie_ids:
        title, genres, release_date, imdb_url = get_movie_details(id)
        recommended_movies.append({'title': title, 'genres': genres, 'release_date': release_date, 'imdb_url': imdb_url})
    # movie = pd.DataFrame(recommended_movies)
    return render_template('results.html', movie_name=movie_name, recommended_movies=recommended_movies)

@app.route('/movies', methods=['GET', 'POST'])
def movie_by_genre():
    selected_genre = request.form.get('genre')
    # TODO: Get a list of movies based on the selected genre
    mv = movies.loc[movies[selected_genre] == 1, 'movie title'].tolist()
    return render_template('index.html', movies=mv, selected_genre=selected_genre, movie_titles=movie_title, movie_genres = movie_genres)


if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')
